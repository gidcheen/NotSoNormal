﻿using UnityEngine;
using System.Collections;

public class IceController : MonoBehaviour 
{
	[Header("Main Slaideris")]
	[Range(0f,1f)]
	public float freezeValue = 0;

	[Space(8)]
	[Header("Settings")]
	public float speed = 1f;
	[Range(0f,1f)]
	public float minOpacity = 0f;
	[Range(0f,1f)]
	public float maxOpacity = 1f;
	[Range(0f,1f)]
	public float minDistortion = 0f;
	[Range(0f,1f)]
	public float maxDistortion = 1f;
	[Range(0.1f,5f)]
	public float minMaskLevel = 0.1f;
	[Range(0.1f,5f)]
	public float maxMaskLevel = 5f;

	Renderer rend;
	float distortionLevel;
	float maskLevel;
	float opacity;

	void Start()
	{
		rend = GetComponent<Renderer>();
	}

	void Update()
	{
		float newOpacity = freezeValue.Remap(0f,1f,minOpacity,maxOpacity);
		float newDistortionLevel = freezeValue.Remap(0f,1f,minMaskLevel,maxMaskLevel);
		float newMaskLevel = freezeValue.Remap(0f,1f, 5f, 0.1f);

		opacity = Mathf.Lerp(opacity, newOpacity, speed * Time.deltaTime);
		distortionLevel = Mathf.Lerp(distortionLevel, newDistortionLevel, speed * Time.deltaTime);
		maskLevel = Mathf.Lerp(maskLevel, newMaskLevel, speed * Time.deltaTime);

		RefreshMaterial();
	}

	void RefreshMaterial()
	{
		rend.material.SetFloat("_Opacity", opacity);
		rend.material.SetFloat("_Distortion", distortionLevel);
		rend.material.SetFloat("_Maskeslevelis", maskLevel);
	}


}
