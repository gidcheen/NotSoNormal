﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

  public VideoPlayer vp;

  public float video_length;

  void Start()
  {
    StartCoroutine(CloseGame());
  }

  private IEnumerator CloseGame()
  {
    yield return new WaitForSeconds(video_length);

    SceneManager.LoadScene("Intro");
  }
}
